from ui import Image, load_view
from random import uniform
from time import time
from threading import Timer
from datetime import datetime

now_screen = None
rand = None
time_start = None
timer = None
score = None
high_score = None

def screen_tapped(sender):
    global now_screen
    global rand
    global time_start
    global timer
    global score
    global high_score

    if now_screen == 'blue':
        rand = uniform(1, 5)
        time_start = time()

        change_screen('orange')

        timer = Timer(rand, change_screen, ['green'])
        timer.start()
    elif now_screen == 'orange':
        timer.cancel()
        change_screen('red')
    elif now_screen == 'green':
        score = int((time() - time_start - rand) * 1000)

        open('scores.txt', 'a').close()

        if (high_score is None):
            file = open('scores.txt', 'r')
            list = file.read().split(':')
            if (len(list) >= 2):
                high_score = int(list[1])
                file.close()
            else:
                file = open('scores.txt', 'w')
                file.write(datetime.now().strftime("%s:") + str(score))
                file.close()

                high_score = score
        if (score < high_score):
            file = open('scores.txt', 'w')
            file.write(datetime.now().strftime("%s:") + str(score))
            file.close()

            high_score = score

        change_screen('yellow')
    elif now_screen == 'yellow' or now_screen == 'red':
        change_screen('blue')

def change_screen(screen):
    global now_screen

    label1 = view['label1']
    label2 = view['label2']
    imageview1 = view['imageview1']

    if screen == 'blue':
        view.background_color = '#3498db'
        label1.text = 'Tap to start'
        label2.text = 'When the orange screen turns green, tap as quickly as you can.'
        imageview1.image = Image.named(
        'Bolt.png')
    elif screen == 'red':
        view.background_color = '#e74c3c'
        label1.text = 'Too soon!'
        label2.text = 'Tap to try again.'
        imageview1.image = Image.named(
        'Warning.png')
    elif screen == 'green':
        view.background_color = '#2ecc71'
        label1.text = 'Tap!'
        label2.text = ''
        imageview1.image = Image.named(
        'Tap.png')
    elif screen == 'yellow':
        view.background_color = '#f1c40f'
        label1.text = str(score) + 'ms'
        if (score <= high_score):
            label2.text = 'Congratulations! This is your highscore!'
        else:
            label2.text = 'Your highscore: ' + str(high_score) + 'ms'
        imageview1.image = Image.named(
        'Time.png')
    elif screen == 'orange':
        view.background_color = '#e67e22'
        label1.text = 'Wait for green'
        label2.text = ''
        imageview1.image = Image.named(
        'Info.png')

    now_screen = screen

if __name__ == '__main__':
    view = load_view()

    change_screen('blue')

    view.present(style='full_screen', orientations=['portrait'], hide_title_bar=True, animated=False)

